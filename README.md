# Terraform Wrapper

If you are tied of runnig appropriate terraform version depends on each project - you need this wrapper. It tries to get terrafom version from versions.tf and run corresponding terraform binary.

Some readme will be created as soon as possible. I think...

## Installation

 This section needs some corrections...

put script in directory, add this directory to the $PATH env variable (or make symlink, you know), download terraform binaries, put them in folder, rename it according to the version, run wrapper.

...look inside script...

## Usage

Run wrapper inside directory with terraform files like regular terraform command.

## Known issues

* Only one constraint - "~>"

## TODO

* Add other constraints.
* Automatically offer to download an appropriate version (I don't believe it's really necessary and I'll do it, but idea is good)

## Contributing



## Authors and acknowledgment

Author: Eduard Kuleshov

## License

Unlicensed... at all.
